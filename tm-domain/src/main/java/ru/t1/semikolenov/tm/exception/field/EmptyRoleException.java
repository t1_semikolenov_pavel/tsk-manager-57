package ru.t1.semikolenov.tm.exception.field;

import ru.t1.semikolenov.tm.exception.AbstractException;

public final class EmptyRoleException extends AbstractException {

    public EmptyRoleException() {
        super("Error! Role is empty...");
    }

}