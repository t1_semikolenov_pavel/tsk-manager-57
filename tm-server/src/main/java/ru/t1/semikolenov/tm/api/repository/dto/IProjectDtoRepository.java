package ru.t1.semikolenov.tm.api.repository.dto;

import ru.t1.semikolenov.tm.dto.model.ProjectDTO;

public interface IProjectDtoRepository extends IUserDtoOwnedRepository<ProjectDTO> {
}
